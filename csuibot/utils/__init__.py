from datetime import date


def lookup_zodiac(month, day):
    zodiacs = [
        Aries()
        # Implement the mandatory task in here
    ]

    for zodiac in zodiacs:
        if zodiac.date_includes(month, day):
            return zodiac.name
    else:
        return 'Unknown zodiac'


def lookup_chinese_zodiac(year):
    num_zodiacs = 12
    zodiacs = {
        0: 'rat'
        # Implement the mandatory task in here
    }
    ix = (year - 4) % num_zodiacs

    try:
        return zodiacs[ix]
    except KeyError:
        return 'Unknown chinese zodiac'