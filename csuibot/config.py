from os import environ


APP_ENV = environ.get('APP_ENV', 'development')
DEBUG = environ.get('DEBUG', 'true') == 'true'
TELEGRAM_BOT_TOKEN = environ.get('TELEGRAM_BOT_TOKEN', '347465688:AAEYgcDot-KGeq6Wop7xmKa1TiXG8ErKP5s')
LOG_LEVEL = environ.get('LOG_LEVEL', 'DEBUG')
WEBHOOK_HOST = environ.get('WEBHOOK_HOST', 'https://jodiak.herokuapp.com//bot')
WEBHOOK_URL = environ.get('WEBHOOK_URL', 'https://jodiak.herokuapp.com/')
